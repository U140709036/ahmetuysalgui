package tr.edu.mu.ceng.gui.bodyindexcalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class bmiCalculationActivity extends AppCompatActivity {
 public Bundle getWeightData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi_calculation);
        final TextView textView_ideal = findViewById(R.id.textView_ideal);
        final TextView textView_condition = findViewById(R.id.textView_condition);
        Button home = findViewById(R.id.homebutton);

        update();



        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent i = new Intent(bmiCalculationActivity.this,MainActivity.class);

                startActivity(i);




            }
        });




    }

    private void update() {


        getWeightData = getIntent().getExtras();
        Boolean isMale = getWeightData.getBoolean("isMale");
        final String height_text = getWeightData.getString("bmiHeight");
        final String weight_text = getWeightData.getString("bmiWeight");


        float ideal_weight_male = (float) (50 + 2.3 * (Float.parseFloat(height_text) * 0.4 - 60));//ideal
        float ideal_weight_female = (float) (45.5 + 2.3 * (Float.parseFloat(String.valueOf(height_text)) * 0.4 - 60));


        double height = (Integer.parseInt(height_text));
        double weight = Integer.parseInt(weight_text);
        double bmi = (weight/(((height*0.01))*(height*0.01)));//user bmi

        final TextView textView_ideal = findViewById(R.id.textView_ideal);
        final TextView textView_condition = findViewById(R.id.textView_condition);



        if (isMale == true) {

            textView_ideal.setText(String.valueOf(ideal_weight_male) + " kg");

            if (bmi <= 18.5) {

                textView_condition.setBackgroundResource(R.drawable.thin);
                textView_condition.setText(R.string.thin);


            } else if (18.5 < bmi && bmi <= 24.9) {

                textView_condition.setBackgroundResource(R.drawable.my_button);
                textView_condition.setText(R.string.ideal);


            } else if (25 < bmi && bmi <= 29.9) {

                textView_condition.setBackgroundResource(R.drawable.surplus);
                textView_condition.setText(R.string.surplusweight);


            } else if (30 < bmi && bmi <= 39.9) {

                textView_condition.setBackgroundResource(R.drawable.overweight);
                textView_condition.setText(R.string.overweight);


            } else if (40 < bmi) {

                //textView_condition.setBackgroundResource(R.color.obese);
                textView_condition.setBackgroundResource(R.drawable.obese);
                textView_condition.setText(R.string.obese);


            }
        } else if (isMale == false) {

            textView_ideal.setText(String.valueOf(ideal_weight_female)+" kg");

            if (bmi <= 18.5) {
                //textView_condition.setBackgroundResource(R.color.thin);
                textView_condition.setBackgroundResource(R.drawable.thin);
                textView_condition.setText(R.string.thin);


            } else if (18.5 < bmi && bmi <= 24.9) {

                //textView_condition.setBackgroundResource(R.color.ideal);
                textView_condition.setBackgroundResource(R.drawable.my_button);
                textView_condition.setText(R.string.ideal);


            } else if (25 < bmi && bmi <= 29.9) {

                //textView_condition.setBackgroundResource(R.color.surplusweight);
                textView_condition.setBackgroundResource(R.drawable.surplus);
                textView_condition.setText(R.string.surplusweight);


            } else if (30 < bmi && bmi <= 39.9) {

                //textView_condition.setBackgroundResource(R.color.overweight);
                textView_condition.setBackgroundResource(R.drawable.overweight);
                textView_condition.setText(R.string.overweight);


            } else if (40 < bmi) {

                //textView_condition.setBackgroundResource(R.color.obese);
                textView_condition.setBackgroundResource(R.drawable.obese);
                textView_condition.setText(R.string.obese);


            }




        }



    }


    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()== android.R.id.home){

            this.finish();
            return true;

        }
        return super.onOptionsItemSelected(item);

    }


}


